---
# Display name
name: G S Nitesh Narayana

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Student Researcher

# Organizations/Affiliations
organizations:
- name: IIITDM Kancheepuram, IIT Madras
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include computer hardware, memory systems and architecture. Typically anything that is in the range of NAND-Kernel interests me. (Working on the above kernel part!!)

interests:
- Computer Organisation and Architecture
- Computer Systems Design and Engineering
- Design Thinking

education:
  courses:
  - course: B.Tech + M.Tech in Computer Engineering
    institution: Indian Institute of Information Technology Design and Manufacturing Kancheepuram
    year: 2016-21
  - course: School Education 
    institution: Delhi Public School Hyderabad 
    year: 2003-16

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/nitesh-narayana-774864131/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/Nitesh8998
- icon: github
  icon_pack: fab
  link: https://github.com/Nitesh8998
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/cv_Nitesh.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

Hi, I am a student researcher at IIITDM Kancheepuram. Part-Time researcher and Teacher Assistant at IIT Madras.

I like to venture in to problems related to memory systems, computer architecture. Typically anything that is in the range of NAND-Kernel interests me (Working on the above kernel part!!). I will pursue a Ph.D in the field of Computer Architecture after my Post-Grad. I like exploring new domains.
