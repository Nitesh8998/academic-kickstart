+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Part-Time Researcher"
  company = "RISE Labs"
  company_url = ""
  location = "IIT Madras"
  date_start = "2019-07-15"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Analysing DRAMs and Controllers
  * Interfacing Modules
  * Working with Simulators
  """

[[experience]]
  title = "Teacher Assistant"
  company = ""
  company_url = ""
  location = "IIT Madras"
  date_start = "2019-08-01"
  date_end = ""
  description = """
  Handling the Lab component for Advance Computer Architecture and Organisation course. Under the guidance of Prof. Madhumutyam
  """

[[experience]]
  title = "Project Intern"
  company = "Shakti Team - RISE Labs"
  company_url = ""
  location = "IIT Madras"
  date_start = "2019-05-13"
  date_end = "2019-07-13"
  description = """
  Was a part of the verification team. Designed DUT interface with simulator API for Shakti Core verification.
  """

[[experience]]

  title = "Research Fellow"
  company = "HPRCSE Lab"
  company_url = ""
  location = "IIITDM Kancheepuram"
  date_start = "2018-12-01"
  date_end = "2019-07-13"
  description = """
  Dealt with research problems related to computer architecture  and network systems.
  """


[[experience]]
  title = "Project Intern"
  company = "CavenTek"
  company_url = ""
  location = "MadeIT IIITDM"
  date_start = "2016-11-01"
  date_end = "2016-12-31"
  description = """Worked on Physical File Tracking System. Developed the UI for it."""

+++
